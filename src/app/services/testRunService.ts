import { Injectable } from 'angular2/angular2';
import { Headers } from 'angular2/http';
import { Constants } from '../utils/constants';
import { HttpService } from './httpService';
import { AuthService } from './authService';
import { TestRun, TestMethod } from '../common/models';

interface ITestRunService {
    upload(xmlFile: File, testCycleId?: number, comment?: string): any;
    getByTestCycleId(testCycleId: number): any;
}

@Injectable()
export class TestRunService implements ITestRunService {

    constructor(private http: HttpService,private authService: AuthService) {
    }

    upload(xmlFile: File, testCycleId?: number, comment?: string): any {
        var formData = new FormData();
        formData.append("testCycleId", testCycleId);
        formData.append("comment", comment);
        formData.append("xmlfile", xmlFile);

        var xmlHttpRequest = new XMLHttpRequest();
        xmlHttpRequest.open("POST", Constants.BASE_URL + 'api/testrun/upload', true);
        xmlHttpRequest.setRequestHeader("Authorization", 'Bearer ' + this.authService.getToken().access_token);
        xmlHttpRequest.send(formData);

        return xmlHttpRequest;
    }

    getByTestCycleId(testCycleId: number): any {
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');

        return this.http.get(Constants.BASE_URL + 'api/TestRun/TestCycle/' + testCycleId, { headers: headers });
    }
}

