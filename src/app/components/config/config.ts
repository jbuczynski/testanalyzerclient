import { Component, View, CORE_DIRECTIVES, FORM_DIRECTIVES, NgIf} from 'angular2/angular2';

interface IConfigController {
}

@Component({
    selector: 'config'
})
@View({
    directives: [CORE_DIRECTIVES, FORM_DIRECTIVES, NgIf],
    templateUrl: "app/components/config/view.html"
})
export class Config implements IConfigController {
    constructor() {
        console.log('Config Start')
    }
}