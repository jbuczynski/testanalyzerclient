import { Component, View } from 'angular2/angular2';
import { TestCycleService } from '../../services/testCycleService';
import { TestRunService } from '../../services/testRunService';
import { ErrorBodyResponse, TestCycle, SelectOption } from '../../common/models';
import { Upload } from '../upload/upload';

@Component({
    selector: 'uplaodTestRun'
})
@View({
    directives: [Upload],
    templateUrl: 'app/components/uploadTestRun/view.html'
})
export class UploadTestRun {
    public testCycleSelectOptions = new Array<SelectOption>();
    public testCycleId:number = -1;
    public comment:string;
    public xmlFile:any;
    public isSuccess:boolean = false;
    public errorlist:string[] = [];

    constructor(private testCycleService:TestCycleService, private testRunService:TestRunService) {
        var self:UploadTestRun = this;
        testCycleService.getTestCycle()
            .map(res => res.json())
            .subscribe(
                data => self.testCycleSelectOptions = data.map((testCycle:TestCycle) => {
                return {
                    name: testCycle.Name,
                    id: testCycle.Id
                };
            }),
                err => console.log('error ' + err),
            () => console.log('getting testCycles completed')
        );
        console.log('Upload Start');
    }

    public upload() {
        this.errorlist = [];
        this.testRunService.upload(this.xmlFile, this.testCycleId, this.comment)
            .onload = (event) => {
            if (event.target.status === 200) {
                this.isSuccess = true;
            } else {
                var body:ErrorBodyResponse = JSON.parse(event.target.response)
                console.log("error occurred during uploading xml: " + JSON.stringify(body));
                this.errorlist.push(body.Message);
            }
        };
    }

    public getFileFromEvent(event):void {
        this.xmlFile = event.target.files[0];
    }
}
