import { Component, View, CORE_DIRECTIVES, NgIf} from 'angular2/angular2';
import { TestComparerService } from '../../services/testComparerService';
import { SelectTestRun } from '../select-test-run/selectTestRun';
import { Config } from '../config/config';
import { CompareGrid } from './compareGrid/compareGrid';

interface ICompareController {
    config:Config;
    testComparerService:TestComparerService;
}

@Component({
    selector: 'compare',
})

@View({
    directives: [CORE_DIRECTIVES, NgIf, SelectTestRun, Config, CompareGrid],
    templateUrl: "app/components/compare/view.html"
})

export class Compare implements ICompareController {
    public config:Config;
    public testComparerService:TestComparerService;

    // Think how to pass params to constructor without using Angular injector. (will be useful in testes)
    constructor() {
        if(!this.config) { this.config = new Config(); };
        if(!this.testComparerService) { this.testComparerService = new TestComparerService(); };
        console.log('Compare Master Start');
    }

    get diagnostic() {
        return 'Comparer MAIN' + JSON.stringify(this.testComparerService);
    }
}