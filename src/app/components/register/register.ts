import { Component, View, CORE_DIRECTIVES, FORM_DIRECTIVES, NgIf } from 'angular2/angular2';
import { AuthService } from '../../services/authService';
import { ErrorBodyResponse, GenericResponse, RegisterBodyRequest } from '../../common/models';

class RegisterForm {
	email: string;
	remail: string;
	password: string;
	repassword: string;
	isSubmitEnable: boolean;
}

interface IRegisterController {
	registerForm: RegisterForm;
	errorlist: string[];
	isSuccess: boolean;
	register(event: any): void;
}

@Component({
    selector: 'register'
})
@View({
	templateUrl: 'app/components/register/view.html',
	directives: [CORE_DIRECTIVES, FORM_DIRECTIVES, NgIf]
})
export class Register implements IRegisterController {
	registerForm = new RegisterForm();
	errorlist: string[] = [];
	isSuccess: boolean = false;

	constructor(private authService: AuthService) {
        console.log('Register Start');
		this.registerForm.isSubmitEnable = true;
    }

	register(): void {
		this.authService.register(this.convertToRegisterBodyRequest())
			.subscribe(
			data => {
				this.handleResponse(data);
			},
			err => console.log('error ' + err),
			() => console.log('Register complete')
			);
	}

	private onKeyUp(event: any): void {
		if (event.target.type === 'email') {
			var msg: string = 'Emails are not this same!';
			if (this.registerForm.email !== this.registerForm.remail) {
				if (!this.isStringExistInErrorList(msg)) {
					this.errorlist.push(msg);
				}
			} else {
				this.errorlist = this.errorlist.filter((el: string) => el !== msg);
			}
		} else if (event.target.type === 'password') {
			var msg: string = 'Passwords are not this same!';
			if (this.registerForm.password !== this.registerForm.repassword) {
				if (!this.isStringExistInErrorList(msg)) {
					this.errorlist.push(msg);
				}
			} else {
				this.errorlist = this.errorlist.filter((el: string) => el !== msg);
			}
		}

		if (this.registerForm.email === this.registerForm.remail &&
			this.registerForm.password === this.registerForm.repassword) {
			this.registerForm.isSubmitEnable = false;
		}
		else {
			this.registerForm.isSubmitEnable = true;
		}
	}

	private isStringExistInErrorList(msg: string): boolean {
		for (var i: number = 0; i < this.errorlist.length; i++) {
			if (this.errorlist[i] === msg) { return true; }
		}
		return false;
	}

	private handleResponse(response: GenericResponse): void {
		if (response.status === 200) {
			console.log("OK - 200");
			this.isSuccess = true;
			// hide register form and dispaly message with link to other (main) component
			// go to other view
		} else {
			console.log("httpstatusCode was = " + response.status);
			if (response._body) {
				var body: ErrorBodyResponse = JSON.parse(response._body);
				console.log(body);
				this.errorlist = body.ModelState[''];
			}
		}
	}

	private convertToRegisterBodyRequest(): RegisterBodyRequest {
		return new RegisterBodyRequest(this.registerForm.email,
			this.registerForm.password,
			this.registerForm.repassword);
	}
}
