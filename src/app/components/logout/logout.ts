import { Component} from 'angular2/angular2';
import { AuthService } from '../../services/authService';

@Component({
    selector: 'logout',
    template: ``
})
export class Logout {
    constructor(public authService:AuthService) {
        authService.logout();
    }
}