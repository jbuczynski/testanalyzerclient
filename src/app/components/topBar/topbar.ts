import { Component, View, NgIf } from 'angular2/angular2';
import { AuthService } from '../../services/authService';
@Component({
    selector: 'topbar'
})
@View({
	templateUrl: 'app/components/topBar/view.html',
    directives: [NgIf]
})
export class TopBar {

	constructor(private authService:AuthService) {
        console.log('TopBar Start');
    }

    isUserLoggedIn():boolean {
        return this.authService.isUserLoggedIn();
    }

    logout():void {
        this.authService.logout();
    }
}
